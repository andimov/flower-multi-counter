'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    uglify = require('gulp-uglify'),
    browserSync = require("browser-sync"),
    reload = browserSync.reload;

var path = {
    build: { //Тут мы укажем куда складывать готовые после сборки файлы
        html: './build/',
        js: 'build/js/'
    },
    src: {
        html: 'pub/*.html',
        js: 'pub/js/*.js'
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        html: 'pub/*.html',
        js: 'pub/js/*.js',
        style: 'pub/css/*.css'
    },
    clean: './build'
};

gulp.task("default", ['build']);

gulp.task('clean', function (cb) {
    require('rimraf')(path.clean, cb);
});

gulp.task('build', ['html:build', 'js:build']);

// Static Server + watching scss/html files
gulp.task('serve', ['build'], function () {
    browserSync.init({server: path.build.html});

    gulp.watch(path.watch.html, ['build']);
    gulp.watch(path.watch.js, ['build']);
    gulp.watch(path.watch.style, ['build']);
});

gulp.task('html:build', function () {
    var htmlmin = require('gulp-htmlmin'),
        gulpif = require('gulp-if'),
        cssmin = require('gulp-clean-css'),
        useref = require('gulp-useref');

    gulp.src(path.src.html)
        .pipe(useref())
        .pipe(gulpif('*.css', cssmin()))
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});

gulp.task('js:build', function () {
    gulp.src(path.src.js)
        .pipe(uglify())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});
